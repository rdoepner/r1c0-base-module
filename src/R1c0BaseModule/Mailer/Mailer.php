<?php

/**
 * Copyright (c) 2014-2015, Rico Döpner
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the names of the copyright holders nor the names of the
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author      Rico Döpner <kontakt@rico-doepner.de>
 * @copyright   2014-2015, Rico Döpner
 * @license     http://www.opensource.org/licenses/bsd-license.php  BSD License
 * @link        http://rico-doepner.de/
 */
namespace R1c0BaseModule\Mailer;

use R1c0BaseModule\Mailer\MailerInterface;
use R1c0BaseModule\Exception\RuntimeException;
use Zend\Mail\Message;
use Zend\Mail\Transport\TransportInterface;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Mime as MimeType;
use Zend\Mime\Part as MimePart;
use Exception;

class Mailer extends Message implements MailerInterface
{

    const ENCODING_UTF8 = "UTF-8";

    protected $encoding = self::ENCODING_UTF8;

    protected $textBody;

    protected $attachments = array();

    protected $transport;

    public function __construct(TransportInterface $transport)
    {
        $this->transport = $transport;
    }

    public function getTextBody()
    {
        return $this->textBody;
    }

    public function setTextBody($textBody)
    {
        $this->textBody = $textBody;
        
        return $this;
    }

    public function addAttachment($targetFilename, $pathOrResource)
    {
        $this->attachments[$targetFilename] = $pathOrResource;
        
        return $this;
    }

    public function setAttachments(array $attachments)
    {
        $this->attachments = $attachments;
        
        return $this;
    }

    public function getAttachments()
    {
        return $this->attachments;
    }

    public function send()
    {
        $mimeMessage = new MimeMessage();
        $mimeMessage->addPart($this->getTextBodyMimePart());
        $attachmentMimeParts = $this->getAttachmentMimeParts();
        
        foreach ($attachmentMimeParts as $mimePart) {
            $mimeMessage->addPart($mimePart);
        }
        
        $this->setBody($mimeMessage);
        $this->setEncoding($this->getEncoding());
        $contentType = $this->getHeaders()->get('Content-Type');
        $contentType->addParameter('charset', $this->getEncoding());
        
        if (! empty($attachmentMimeParts)) {
            $contentType->setType(MimeType::MULTIPART_MIXED);
        } else {
            $contentType->setType(MimeType::TYPE_TEXT);
        }
        
        try {
            $this->transport->send($this);
        } catch (Exception $exception) {
            return false;
        }
        
        return true;
    }

    protected function getTextBodyMimePart()
    {
        $textBody = $this->getTextBody();
        $mimePart = new MimePart($textBody);
        $mimePart->type = MimeType::TYPE_TEXT;
        $mimePart->charset = $this->getEncoding();
        
        return $mimePart;
    }

    protected function getAttachmentMimeParts()
    {
        $mimeParts = array();
        $attachments = $this->getAttachments();
        
        foreach ($attachments as $targetFilename => $pathOrResource) {
            if (! is_resource($pathOrResource)) {
                if ((! file_exists($pathOrResource)) || (! $pathOrResource = fopen($pathOrResource, 'r'))) {
                    throw new RuntimeException('Attachment could not be opened.');
                }
            }
            
            $mimePart = new MimePart($pathOrResource);
            $mimePart->type = MimeType::TYPE_OCTETSTREAM;
            $mimePart->encoding = MimeType::ENCODING_BASE64;
            $mimePart->disposition = MimeType::DISPOSITION_ATTACHMENT;
            $mimePart->filename = $targetFilename;
            $mimeParts[] = $mimePart;
        }
        
        return $mimeParts;
    }
}
